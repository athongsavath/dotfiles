Use 
https://www.atlassian.com/git/tutorials/dotfiles

For every coc-* file, run the command:
- :CocInstall coc-explorer
- :CocInstall coc-tsserver
- :CocInstall coc-python

Install the following:
- sudo apt install ripgrep
- sudo apt-get install ctags
- sudo apt install silversearcher-ag
- sudo apt install universal-ctags

brew install the_silver_searcher

Install VimPlug using
https://github.com/junegunn/vim-plug
```
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```



## Adding path to fish
fish_add_path ~/.npm-global/bin
### [When permission denied in NPM global installs](https://stackoverflow.com/questions/48910876/error-eacces-permission-denied-access-usr-local-lib-node-modules/55274930#55274930)
