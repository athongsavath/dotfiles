call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'vimwiki/vimwiki'

" Vim in browser
" Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-rhubarb'

Plug 'wellle/targets.vim'

" Go
Plug 'fatih/vim-go'

" GraphQL
Plug 'jparise/vim-graphql'

" Markdown
Plug 'godlygeek/tabular' | Plug 'plasticboy/vim-markdown'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install' }

"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'rust-lang/rust.vim'
Plug 'rhysd/vim-clang-format'
Plug 'dag/vim-fish'
Plug 'neoclide/coc-eslint'
"Plug 'ctrlpvim/ctrlp.vim' " fuzzy finder
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-rooter'

Plug 'christoomey/vim-tmux-navigator'
Plug 'vim-airline/vim-airline'

Plug 'styled-components/vim-styled-components', { 'branch': 'main' }

" JavaScript
Plug 'pangloss/vim-javascript'
Plug 'maxmellon/vim-jsx-pretty'

"TypeScript
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'

" Haskell
Plug 'neovimhaskell/haskell-vim'
Plug 'alx741/vim-hindent' 

Plug 'airblade/vim-gitgutter'
"Plug 'mg979/vim-visual-multi', {'branch': 'master'}

" Comments
Plug 'scrooloose/nerdcommenter'

" Automatic Closing tag html
Plug 'alvan/vim-closetag'

" ctags
Plug 'ludovicchabant/vim-gutentags'

Plug 'dracula/vim', { 'name': 'dracula' }
Plug 'ryanoasis/vim-devicons'
call plug#end()

" Autoinstall all plugs
if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  autocmd VimEnter * PlugInstall
endif

" Vimwiki
let g:vimwiki_list = [{'syntax': 'markdown', 'ext': '.md'}]
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
let g:vimwiki_markdown_list_ext = 1
let g:taskwiki_markup_syntax = 'markdown'
let g:markdown_folding = 1

colorscheme dracula

syntax on
syntax enable

set termguicolors

set relativenumber
set ttyfast
set number
set colorcolumn=80

let g:rustfmt_autosave = 1
let g:rustfmt_emit_files = 1
let g:rustfmt_fail_silently = 0

" No cheating
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" Easier Split Navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-w>x <C-w>x<C-w>l

set splitbelow
set splitright

filetype plugin indent on
set autoindent
set encoding=UTF-8


set tabstop=2
set shiftwidth=2
set expandtab

set foldmethod=manual
map <Space> <Leader>


"Automatically insert closing curly brace
inoremap {<CR> {<CR>}<C-o>O

nmap <leader>gd <Plug>(coc-definition)
nmap <leader>gr <Plug>(coc-references)
nnoremap <C-p> :GFiles<CR>

" Use fontawesome icons as signs
let g:gitgutter_sign_added = '+'
let g:gitgutter_sign_modified = '>'
let g:gitgutter_sign_removed = '-'
let g:gitgutter_sign_removed_first_line = '^'
let g:gitgutter_sign_modified_removed = '<'


" Vim Fugitive
nmap <space>gh :diffget //3<CR>
nmap <space>gf :diffget //2<CR>
nmap <space>gs :G<CR>
:set diffopt+=vertical

"let g:LanguageClient_serverCommands = {
"   \ 'c': ['ccls', '--log-file=/tmp/cc.log'],
"   \ 'cpp': ['ccls', '--log-file=/tmp/cc.log'],
"   \ 'cuda': ['ccls', '--log-file=/tmp/cc.log'],
"   \ 'objc': ['ccls', '--log-file=/tmp/cc.log'],
"   \ }

"let g:ale_linters = {
    "\ 'python': ['pylint'],
    "\ 'vim': ['vint'],
    "\ 'cpp': ['clang'],
    "\ 'c': ['clang']
"\}
"
" custom setting for clangformat
let g:neoformat_cpp_clangformat = {
    \ 'exe': 'clang-format',
    \ 'args': ['--style="{IndentWidth: 4}"']
\}
let g:neoformat_enabled_cpp = ['clangformat']
let g:neoformat_enabled_c = ['clangformat']

" coc config
let g:coc_global_extensions = [
  \ 'coc-snippets',
  "\ 'coc-pairs',
  \ 'coc-tsserver',
  \ 'coc-eslint',
  \ 'coc-prettier',
  \ 'coc-json',
  \ 'coc-css',
  \ 'coc-html',
  \ 'coc-emmet'
  \ ]

let g:airline#extensions#whiteplace#enabled = 0
let g:airline#extensions#branch#enabled = 0
let g:airline_skip_empty_sections = 1

function! SplitIfNotOpen(...)
    let fname = a:1
    let call = ''
    if a:0 == 2
	let fname = a:2
	let call = a:1
    endif
    let bufnum=bufnr(expand(fname))
    let winnum=bufwinnr(bufnum)
    if winnum != -1
	" Jump to existing split
	exe winnum . "wincmd w"
    else
	" Make new split as usual
	exe "vsplit " . fname
    endif
    " Execute the cursor movement command
    exe call
endfunction

command! -nargs=+ CocSplitIfNotOpen :call SplitIfNotOpen(<f-args>)


let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
" For coc-pairs
"let b:coc_pairs_disabled = ["'", "<"]

" Comments with nerdcommenter
nmap <C-_>   <Plug>NERDCommenterToggle
vmap <C-_>   <Plug>NERDCommenterToggle<CR>gv

" HTML
let g:closetag_filenames = '*.html,*.jsx,*.js,*.tsx,*.ts'
let g:closetag_shortcut = '>'

" Explorer
nmap <space>e :CocCommand explorer<CR>
autocmd BufEnter * if (winnr("$") == 1 && &filetype == 'coc-explorer') | q | endif

"let g:LanguageClient_loadSettings = 1 " Use an absolute configuration path if you want system-wide settings
"let g:LanguageClient_settingsPath = '/home/YOUR_USERNAME/.config/nvim/settings.json'
" https://github.com/autozimu/LanguageClient-neovim/issues/379 LSP snippet is not supported
"let g:LanguageClient_hasSnippetSupport = 0
"
"
"
"

" tsconfig.json is actually jsonc, help TypeScript set the correct filetype
" https://github.com/neoclide/coc-json/issues/11
autocmd BufRead,BufNewFile tsconfig.json set filetype=jsonc
"autocmd BufNewFile,BufRead *.ts,*.tsx setlocal filetype=typescript
"=================================================================================
" Competitive programming stuff
function CP()
    :0read ~/.config/nvim/templates/competitive_programming.cpp
endfunction


"https://www.reddit.com/r/neovim/comments/hbljvc/setting_up_neovim_for_competitive_programming/fvbmzik/?utm_source=reddit&utm_medium=web2x&context=3
set mp=g++\ -O2\ -Wall\ --std=c++11\ -Wno-unused-result\ %:r.cpp\ -o\ %:r
nmap <F2> :vs %:r.in <CR>
nmap <F8> :!time ./%:r < %:r.in <CR> 
nmap <F9> :w <CR> :make<CR>

"autocmd filetype cpp nnoremap <F5> :w <bar> !g++ -ulimit -Wall -Wno-unused-result -std=c++11   -O2   % -o %:r && ./%:r <CR>
"autocmd filetype cpp nnoremap <F9> :w <bar> !g++ -g -std=c++17 -O0 -Wall % -o %:r && ./%:r <CR>
"=================================================================================
source $HOME/.config/nvim/plug-config/coc.vim
source $HOME/.config/nvim/plug-config/fzf.vim
