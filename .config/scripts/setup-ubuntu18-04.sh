#!/bin/bash

sudo apt-get update

# Node
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt install nodejs
sudo apt install build-essential

# Snap
sudo apt install snapd
echo 'export PATH=$PATH:/snap/bin' >> ~/.bashrc
# Need to `source ~/.bashrc`

# Neovim
sudo add-apt-repository ppa:neovim-ppa/stable
sudo apt-get update
sudo apt-get install neovim

# ccls for Neovim
sudo snap install ccls --classic
sudo snap install ripgrep --classic



