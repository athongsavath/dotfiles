#!/bin/bash

sudo apt-get update

# Installing Neovim
sudo apt-get install neovim git fish tmux curl

chsh -s /usr/bin/fish

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

